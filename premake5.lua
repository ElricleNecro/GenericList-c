newoption(  -- luacheck: globals newoption
	{
		trigger="install-prefix",
		value="DIR",
		description="Directory used to install bin, lib and share directory.",
	}
)

if not _OPTIONS["install-prefix"] then  -- luacheck: globals _OPTIONS
	_OPTIONS["install-prefix"] = os.getenv("HOME") .. "/.local/"
end

solution("CList")  -- luacheck: globals solution
	configurations({"debug", "release"})  -- luacheck: globals configurations
		buildoptions(  -- luacheck: globals buildoptions
			{
				"-std=c11"
			}
		)

		flags(  -- luacheck: globals flags
			{
				"ExtraWarnings"
			}
		)

		defines(  -- luacheck: globals defines
			{
				"_GNU_SOURCE"
			}
		)

		includedirs(  -- luacheck: globals includedirs
			{
				"include/",
			}
		)

	configuration("release")  -- luacheck: globals configuration
		buildoptions(
			{
				"-O3"
			}
		)

	configuration("debug")
		buildoptions(
			{
				"-g3"
			}
		)
		flags(
			{
				"Symbols"
			}
		)

	project("clist")  -- luacheck: globals project
		language("C")  -- luacheck: globals language
		kind("SharedLib")  -- luacheck: globals kind

		location("build/lib")  -- luacheck: globals location
		targetdir("build/lib")  -- luacheck: globals targetdir

		files(  -- luacheck: globals files
			{
				"src/*.c"
			}
		)

		-- defines(
			-- {
				-- define_ptrace,
				-- "LUA_COMPAT_APIINTCASTS"
			-- }
		-- )

		-- libdirs(
			-- {
				-- "build/lib"
			-- }
		-- )

		-- links(
			-- {
				-- "ParseArgsC",
				-- "logger",
				-- "dict",
				-- "lua",
				-- "m"
			-- }
		-- )
