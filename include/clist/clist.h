#ifndef CLIST_H_URKHZYDS
#define CLIST_H_URKHZYDS

#include <stdlib.h>
#include <stdbool.h>

typedef struct clist_t {
	struct clist_t *next;
	bool to_free;
	void *data;
} *CList;

CList clist_new(void);
void  clist_delete(CList obj);

CList clist_append(CList obj, void *data, bool freed);
CList clist_append_default(CList obj, void *data);
CList clist_append_double(CList obj, double data);
CList clist_append_float(CList obj, float data);
CList clist_append_short(CList obj, short data);
CList clist_append_long(CList obj, long data);
CList clist_append_char(CList obj, char data);
CList clist_append_int(CList obj, int data);
CList clist_append_ushort(CList obj, unsigned short data);
CList clist_append_ulong(CList obj, unsigned long data);
CList clist_append_uchar(CList obj, unsigned char data);
CList clist_append_uint(CList obj, unsigned int data);

#define clist_append(obj, x) _Generic( \
		(x), \
		double: clist_append_double, \
		float: clist_append_float, \
		short: clist_append_short, \
		long: clist_append_long, \
		char: clist_append_char, \
		int: clist_append_int, \
		unsigned short: clist_append_ushort, \
		unsigned long: clist_append_ulong, \
		unsigned char: clist_append_uchar, \
		unsigned int: clist_append_uint, \
		default: clist_append_default \
	)(obj, x)

void* clist_pop_default(CList *obj);
// double clist_pop_double(CList obj);
// float clist_pop_float(CList obj);
// short clist_pop_short(CList obj);
// long clist_pop_long(CList obj);
// char clist_pop_char(CList obj);
// int clist_pop_int(CList obj);
// unsigned short clist_pop_ushort(CList obj);
// unsigned long clist_pop_ulong(CList obj);
// unsigned char clist_pop_uchar(CList obj);
// unsigned int clist_pop_uint(CList obj);

// #define clist_pop(obj, x) _Generic( \
		// (x), \
		// double: clist_pop_double, \
		// float: clist_pop_float, \
		// short: clist_pop_short, \
		// long: clist_pop_long, \
		// char: clist_pop_char, \
		// int: clist_pop_int, \
		// unsigned short: clist_pop_ushort, \
		// unsigned long: clist_pop_ulong, \
		// unsigned char: clist_pop_uchar, \
		// unsigned int: clist_pop_uint, \
		// default: clist_pop_default \
	// )(obj)

#endif /* end of include guard: CLIST_H_URKHZYDS */
