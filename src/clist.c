#include "clist/clist.h"

CList clist_new(void) {
	CList obj = NULL;

	if( (obj = malloc(sizeof(struct clist_t))) == NULL ) {
		return NULL;
	}

	obj->next = NULL;

	return obj;
}

void clist_delete(CList obj) {
	while( obj != NULL ) {
		if( obj->to_free )
			free(obj->data);

		CList tmp = obj;
		obj = obj->next;
		free(tmp);
	}
}

CList clist_append(CList obj, void *data, bool freed) {
	CList last = obj;

	while( last->next != NULL )
		last = last->next;

	last = last->next = clist_new();

	last->to_free = freed;
	last->data = data;

	return obj;
}

CList clist_append_default(CList obj, void *data) {
	return clist_append(obj, data, false);
}

CList clist_append_double(CList obj, double data) {
	double *d = NULL;

	if( (d = malloc(sizeof(double))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_float(CList obj, float data) {
	float *d = NULL;

	if( (d = malloc(sizeof(float))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_short(CList obj, short data) {
	short *d = NULL;

	if( (d = malloc(sizeof(short))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_long(CList obj, long data) {
	long *d = NULL;

	if( (d = malloc(sizeof(long))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_char(CList obj, char data) {
	char *d = NULL;

	if( (d = malloc(sizeof(char))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_int(CList obj, int data) {
	int *d = NULL;

	if( (d = malloc(sizeof(int))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_ushort(CList obj, unsigned short data) {
	unsigned short *d = NULL;

	if( (d = malloc(sizeof(unsigned short))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_ulong(CList obj, unsigned long data) {
	unsigned long *d = NULL;

	if( (d = malloc(sizeof(unsigned long))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_uchar(CList obj, unsigned char data) {
	unsigned char *d = NULL;

	if( (d = malloc(sizeof(unsigned char))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

CList clist_append_uint(CList obj, unsigned int data) {
	unsigned int *d = NULL;

	if( (d = malloc(sizeof(unsigned int))) == NULL )
		return NULL;

	*d = data;

	return clist_append(obj, d, true);
}

void* clist_pop_default(CList *obj) {
	CList tmp = *obj;
	*obj = (*obj)->next;

	return tmp->data;
}
